/*
  Ejercicio: Realización de una aplicación cliente servidor en JS
  Autor: Veselin Georgiev | veselingp@hotmail.com
  Descripción: API REST sencilla que almacena datos de una persona
*/
// Inicializamos módulos a utilizar
var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var jade = require('jade');
var restler = require('restler');
var IP = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var PORT = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var app = express();
// Agregamos configuración
app.set('views', __dirname + '/vistas');
app.set('view engine', 'jade');
app.set('IP', IP);
app.set('PORT', PORT);
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(express.static(__dirname + '/public'));
app.use('/', require('./rutas/publico.js'));
app.use('/api', require('./rutas/api.js'));
app.listen(PORT, IP);
console.log('Servidor ejecutándose en: http://' + IP + ':' + PORT + '/');