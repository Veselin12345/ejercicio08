$(document).ready(function(){
	// Presentamos usuarios
	usuarios();
	// Agregamos eventos a botones
	$('#agregar').click(usuarioAgregar); //Agregar
	$('#editar').click(usuarioEditar); //Editar
	$('#eliminar').click(usuarioEliminar); //Eliminar
	$('#guardar').click(usuarioGuardar); //Guardar
	$('#borrar').click(usuarioBorrar); //Borrar
	$('#cancelar').click(usuarioCancelar); //Cancelar
});
function usuarios(){
	// Presentamos usuarios
	$.ajax({ //GET
		url: '/api/usuarios/',
		dataType: 'json',
		type: 'get',
		cache: false,
		timeout: 5000,
		success: function(response) {
			// Presentamos contenido
			$('#tabla tbody').hide(0);
			$('#tabla tbody').html('');
			for(i in response)
				$('#tabla tbody').append('<tr><td><input type="radio", name="lokid", value=' + response[i].$loki + '></td><td>' + response[i].$loki + '</td><td>' + response[i].nombre + '</td><td>' + response[i].edad + '</td></tr>');
			$('#tabla tbody').show('slow');
		},
		error: function(error) {
			$('#mensaje').html(error.mensaje);
		}
	});
}
function usuarioAgregar(){
	// Presentamos cuadro de edición
	usuarioBorrarLista();
	$("#editarModal").modal();
}
function usuarioEditar(){
	// Presentamos cuadro de edición con usuario
	var lokid = $('input[name=lokid]:checked').val();
	if($('input[name=lokid]:checked').val()>0){
		usuarioBorrarLista();
		$.ajax({ //GET
			url: '/api/usuarios/' + lokid,
			dataType: 'json',
			type: 'get',
			cache: false,
			timeout: 5000,
			success: function(response) {
				// Presentamos contenido
				$('#loki').val(response.$loki);
				$('#nombre').val(response.nombre);
				$('#edad').val(response.edad);
			},
			error: function(error) {
				$('#mensaje').html(error.mensaje);
			}
		});
		$("#editarModal").modal();
	}
	else
		$('#mensaje').html('Por favor selecciona un usuario');
}
function usuarioEliminar(){
	// Eliminamos usuario
	var lokid = $('input[name=lokid]:checked').val();
	if($('input[name=lokid]:checked').val()>0){
		usuarioBorrarLista();
		$.ajax({ //DELETE
			url: '/api/usuarios/' + lokid,
			dataType: 'json',
			type: 'delete',
			cache: false,
			timeout: 5000,
			success: function(response) {
				// Presentamos contenido
				$('#mensaje').html('Usuario eliminado');
			},
			error: function(error) {
				$('#mensaje').html(error.mensaje);
			}
		});
		usuarios();
	}
	else
		$('#mensaje').html('Por favor selecciona un usuario');
}
function usuarioGuardar(){
	// Guardaamos usuario
	if($('#nombre').val() == '')
		$('#validar').html('Por favor ingresa el nombre');
	else if($('#edad').val() == '')
		$('#validar').html('Por favor ingresa la edad');
	else{
		var loki = $('#loki').val();
		var type = 'POST';
		if(loki.length > 0)
			type = 'PUT';
		$.ajax({ //POST //PUT
			url: '/api/usuarios/' + loki,
			dataType: 'json',
			type: type,
			cache: false,
			timeout: 5000,
			data: {
				nombre: $('#nombre').val(),
				edad: $('#edad').val()
			},
			success: function(response) {
				$('#mensaje').html('Usuario guardado');
				usuarios();
				usuarioCancelar();
			},
			error: function(error) {
				$('#mensaje').html(error.mensaje);
				usuarios();
				usuarioCancelar();
			}
		});
	}
}
function usuarioBorrarLista(){
	// Borramos contenido establecido
	$('#mensaje').html('');
	$("input[name=lokid]").removeAttr("checked");
}
function usuarioBorrar(){
	// Borramos contenido establecido
	$('#loki').val('');
	$('#nombre').val('');
	$('#edad').val('');
	$('#validar').html('');
}
function usuarioCancelar(){
	// Cancelamos cuadro de edición
	usuarioBorrar();
	$.modal.close();
}