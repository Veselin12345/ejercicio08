// Inicializamos módulos a utilizar
var apiRouter = require('express').Router();
var UsuarioController = require('../controladores/usuario.js');
// Creamos rutas a utilizar y su controlador
apiRouter.route('/usuarios')
.get(UsuarioController.getUsuarios) //GET
.post(UsuarioController.postUsuario) //POST
apiRouter.route('/usuarios/:id')
.get(UsuarioController.getUsuario) //GET
.put(UsuarioController.putUsuario) //PUT
.delete(UsuarioController.deleteUsuario); //DELETE
module.exports = apiRouter;