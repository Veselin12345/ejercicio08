// Inicializamos módulos a utilizar
var publicoRouter = require('express').Router();
var PublicoController = require('../controladores/publico.js');
// Creamos rutas a utilizar y su controlador
publicoRouter.route('/')
.get(PublicoController.getIndex); //GET
module.exports = publicoRouter;